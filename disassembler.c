#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void DisassembleINVALID(uint8_t *codebuffer, int pc){
    uint8_t *code = &codebuffer[pc];
    uint8_t firstnib = (code[0] >> 4);

    printf("%04x %02x %02x ", pc, code[0], code[1]);
    switch (firstnib){
        case 0x00: {
              if(code[1] == 0xEE)
                printf("INVALID");
              if(code[1] == 0xE0)
                printf("INVALID");
            }
            break;
        case 0x01: {
                uint8_t addresshi = code[0] & 0x0f;
                printf("%-3s 0x%01x%02x", "INVALID", addresshi, code[1]);
            }
            break;
        case 0x02: {
                uint8_t addresshi = code[0] & 0x0f;
                printf("%-3s 0x%01x%02x", "INVALID", addresshi, code[1]);
            }
            break;
        case 0x03: {
                uint8_t reg = code[0] & 0x0f;
                printf("%-3s V%01X,0x%02x", "INVALID", reg, code[1]);
            }
            break;
        case 0x04: {
                uint8_t reg = code[0] & 0x0f;
                printf("%-3s V%01X,0x%02x", "INVALID", reg, code[1]);
            }
            break;
        case 0x05: {
                uint8_t reg = code[0] & 0x0f;
                uint8_t reg2 = (code[1] & 0xf0) >> 4;
                printf("%-3s V%01X,V%01X", "INVALID", reg, reg2);
            }
            break;
        case 0x06:{
                uint8_t reg = code[0] & 0x0f;
                printf("%-3s V%01X,0x%02x", "INVALID", reg, code[1]);
            }
            break;
        case 0x07:{
                uint8_t reg = code[0] & 0x0f;
                uint8_t value = code[1];
                printf("%-3s V%01X,0x%02x", "INVALID", reg, code[1]);
            }
            break;
        case 0x08:{
                uint8_t reg = code[0] & 0x0f;
                uint8_t reg2 = (code[1] & 0xf0) >> 4;
                uint8_t action = code[1] & 0x0f;
                // printf("reg: %x, reg2; %x, action: %x\n", reg, reg2, action);
                switch(action){
                  case 0x0: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x1: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x2: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x3: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x4: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x5: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0x6: printf("%-3s V%01X", "INVALID", reg); break;
                  case 0x7: printf("%-3s V%01X,V%01X", "INVALID", reg, reg2); break;
                  case 0xE: printf("%-3s V%01X", "INVALID", reg); break;
                  default: printf("**** Unknown Action Which is always INVALID*****");
                }

            }
            break;
        case 0x09:  {
                uint8_t reg = code[0] & 0x0f;
                uint8_t reg2 = (code[1] & 0xf0) >> 4;
                printf("%-3s V%01X,V%01X", "INVALID", reg, code[1]);
            }
            break;
        case 0x0a:{
                uint8_t addresshi = code[0] & 0x0f;
                printf("%-3s I,0x%01x%02x", "INVALID", addresshi, code[1]);
            }
            break;
        case 0x0b: {
                uint8_t addresshi = code[0] & 0x0f;
                printf("%-3s V0,0x%01x%02x", "INVALID", addresshi, code[1]);
            }
            break;
        case 0x0c: {
                uint8_t reg = code[0] & 0x0f;
                uint8_t value = code[1];
                printf("%-3s V%01X,0x%02x", "INVALID", reg, code[1]);
            }
            break;
        case 0x0d:  {
                uint8_t reg = code[0] & 0x0f;
                uint8_t reg2 = (code[1] & 0xf0) >> 4;
                uint8_t sheight = (code[1] & 0x0f);
                printf("%-3s V%01X,V%01X,%01X", "INVALID", reg, reg2, sheight);
            }
            break;
        case 0x0e:{
            uint8_t reg = code[0] & 0x0f;
            uint8_t action = code[1];

            switch(action){
              case 0x9E: printf("%-3s V%01X", "INVALID", reg); break;
              case 0xA1: printf("%-3s V%01X", "INVALID", reg); break;

              default:
                printf("******** Unkonwn Action which is always INVALID *******");
            }
          }
          break;
        case 0x0f:{
                uint8_t reg = code[0] & 0x0f;
                uint8_t action = code[1];

                switch(action){
                  case 0x55: printf("%-3s [I],V%01X", "INVALID", reg); break;
                  case 0x65: printf("%-3s V%01X, [I]", "INVALID", reg); break;
                  case 0x1E: printf("%-3s I,V%01X", "INVALID", reg); break;
                  case 0x0A: printf("%-3s V%01X, Key", "INVALID", reg); break;
                  case 0x15: printf("%-3s DT,V%01X", "INVALID", reg); break;
                  case 0x18: printf("%-3s ST,V%01X", "INVALID", reg); break;
                  case 0x07: printf("%-3s V%01X, DT", "INVALID", reg); break;
                  case 0x33: printf("%-3s B, V%01X", "INVALID", reg); break;
                  case 0x29: printf("%-3s I,V%01X", "INVALID", reg); break;
                  default:
                  printf("****** Action Unknown which is always INVALID *****");

                }
            }
    }
}





int main (int argc, char**argv){
  FILE *f= fopen(argv[1], "rb");
  if (f==NULL){
      printf("error: Couldn't open %s\n", argv[1]);
      exit(1);
  }

  //Get the file size
  fseek(f, 0L, SEEK_END);
  int fsize = ftell(f);
  fseek(f, 0L, SEEK_SET);

  //INVALID convention puts programs in memory at 0x200
  // They will all have hardcoded addresses expecting that
  //
  //Read the file into memory at 0x200 and close it.
  unsigned char *buffer=malloc(fsize+0x200);
  fread(buffer+0x200, fsize, 1, f);
  fclose(f);

  int pc = 0x200;
  while (pc < (fsize+0x200)){
      DisassembleINVALID(buffer, pc);
      pc += 2;
      printf ("\n");
  }
  return 0;
}
